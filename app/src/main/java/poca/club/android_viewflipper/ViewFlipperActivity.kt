package poca.club.android_viewflipper

import android.app.ActionBar
import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.animation.AnimationUtils
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.ViewFlipper


class ViewFlipperActivity :Activity(),android.view.GestureDetector.OnGestureListener {

    override fun onShowPress(motionEvent: MotionEvent) {

    }

    override fun onSingleTapUp(motionEvent: MotionEvent): Boolean {
        return false
    }

    override fun onDown(motionEvent: MotionEvent): Boolean {
        return false
    }

    override fun onFling(p1: MotionEvent, p2: MotionEvent, v1: Float, v2: Float): Boolean {
        if(p2.x - p1.x > 120){  // 從左向右滑動 （左進右出）
            Log.d(TAG, "從左向右滑動 （左進右出）")
            val rInAnim = AnimationUtils.loadAnimation(activity,R.anim.push_right_in) // 向右滑动左侧进入的渐变效果（alpha  0.1 -> 1.0）
            val rOutAnim = AnimationUtils.loadAnimation(activity,R.anim.push_right_out) // 向右滑动右侧滑出的渐变效果（alpha 1.0  -> 0.1）

            viewFilpper!!.inAnimation = rInAnim
            viewFilpper!!.inAnimation = rOutAnim
            viewFilpper!!.showPrevious()
            return true
        }else if(p2.x - p1.x < -120){ // 從右向左滑動 （右進左出）
            Log.d(TAG, "從右向左滑動 （右進左出）")
            val  lInAnim = AnimationUtils.loadAnimation(activity,R.anim.push_left_in)
            val lOutAnim = AnimationUtils.loadAnimation(activity,R.anim.push_left_out)

            viewFilpper!!.inAnimation = lInAnim
            viewFilpper!!.outAnimation = lOutAnim
            viewFilpper!!.showNext()
            return true
        }
        return true
    }

    override fun onScroll(p1: MotionEvent,p2: MotionEvent, v1: Float, v2: Float): Boolean {
      return false
    }

    override fun onLongPress(motionEvent: MotionEvent) {

    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        viewFilpper!!.stopFlipping()
        viewFilpper!!.isAutoStart = false
        return gestureDetector!!.onTouchEvent(event)

    }

    private val imgs = intArrayOf(R.drawable.img01, R.drawable.img02 , R.drawable.img03, R.drawable.img04, R.drawable.img05);
    private var gestureDetector : GestureDetector? = null
    private var viewFilpper : ViewFlipper? = null
    private var activity : Activity? = null;
    private val TAG : String = "ViewFlipperActivity";



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewFilpper = findViewById(R.id.viewflipper)

        activity = this

        gestureDetector = GestureDetector(this)

        var i = 0;
        do {
            Log.d(TAG,i.toString());
            val iv = ImageView(this)
            iv.setImageResource(imgs[i])
            iv.scaleType = ImageView.ScaleType.FIT_CENTER
            viewFilpper!!.addView(iv,FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,FrameLayout.LayoutParams.MATCH_PARENT))
            i++
        }while (i<imgs.size)

        viewFilpper!!.isAutoStart = false
        viewFilpper!!.setFlipInterval(3000)
        if(viewFilpper!!.isAutoStart && !viewFilpper!!.isFlipping){
            viewFilpper!!.startFlipping()
        }
    }
}
